//
//  ViewController.swift
//  Hesap
//
//  Created by MacBook Pro on 15.04.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var ifFrist = true
    var operatorString = "+-*/="
    
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func buttonTapped(_ sender: Any) {
        if let button = sender as? UIButton{
            addToLabel(value: button.titleLabel!.text!)
            
        }
        
    }
    
    func addToLabel(value: String){
        if let labelArray : [Character] = label.text!.map({ $0
            return $0
        }){
            if operatorString.contains(labelArray.last!) {
                if operatorString.contains(value) {
                    return
                }
            }
        }
        
        if operatorString.contains(value) && ifFrist{
            return
        }else if ifFrist{
            label.text = "\(value)"
            self.ifFrist = false
        }else if value == "=" {
            self.ifFrist = true
            calculate()
            
        }else {
            label.text = label.text! + "\(value)"
        }
    }
    
    func calculate(){
        var numberArray : [String] = (label.text?.components(separatedBy: ["+", "-", "/", "*"]))!
        
        let operatorArray: [String] = (label.text?.components(separatedBy: CharacterSet.decimalDigits).joined().map({ $0
            return "\($0)"
        }))!
        
        if var totalValue = Int(numberArray.first!) {
            numberArray.removeFirst()
            for (number, calcOperator) in zip(numberArray, operatorArray) {
                if calcOperator == "+"{
                    totalValue = totalValue + Int(number)!
                }else if calcOperator == "-" {
                    totalValue = totalValue - Int(number)!
                }else if calcOperator == "/" {
                    totalValue = totalValue / Int(number)!
                }else if calcOperator == "*" {
                    totalValue = totalValue * Int(number)!
                }
            }
            label.text = "\(totalValue)"
        }
        
        print(numberArray)
        print(operatorArray)
    }
}

